package com.example.gestionetudiant.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Candidat {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long codeCandidat;

    private Long telephone;

    private String sex;
    private String email;
    private String lastname;
    private String firstname;

    private Date dateCreation;
    private Date dateModification;
    private Date birthdate;

    private Boolean statut;

    private String matricule = null;
    private String password = null;

    private String role = "etudiant";
}
