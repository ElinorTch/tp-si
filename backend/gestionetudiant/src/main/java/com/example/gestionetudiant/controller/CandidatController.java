package com.example.gestionetudiant.controller;

import com.example.gestionetudiant.models.Candidat;
import com.example.gestionetudiant.service.CandidatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("http://localhost:4200")
@RequestMapping("api/candidat")
public class CandidatController {

    @Autowired
    private CandidatService candidatService;

    @GetMapping("/get")
    public List<Candidat> getAllCandidat() {
        return candidatService.getAllCandidat();
    }

    @GetMapping("/{codeCandidat}")
    public Candidat getCandidatByCode(
            @PathVariable(value = "codeCandidat") Long codeCandidat
    ) {
        return candidatService.getCandidatByCode(codeCandidat);
    }

    @PostMapping("/create")
    public ResponseEntity<String> createCandidat(
            @RequestBody Candidat candidat
    ) {
        return candidatService.addCandidat(candidat);
    }

    @PutMapping("/update")
    public String updateCandidat(
            @RequestBody Candidat candidat
    ) {
        return candidatService.updateCandidat(candidat);
    }

    @DeleteMapping ("/deleteCandidat/{codeCandidat}")
    public String deleteCandidat(
            @PathVariable(value = "codeCandidat") Long codeCandidat
    ) {
        return candidatService.deleteCandidat(codeCandidat);
    }
}
