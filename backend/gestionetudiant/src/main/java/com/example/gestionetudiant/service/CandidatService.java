package com.example.gestionetudiant.service;

import com.example.gestionetudiant.models.Candidat;
import com.example.gestionetudiant.repository.CandidatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CandidatService {

    @Autowired
    private CandidatRepository candidatRepository;

    public List<Candidat> getAllCandidat() {
        return candidatRepository.findAll();
    }

    public Candidat getCandidatByCode(Long codeCandidat) {
        if (candidatRepository.findById(codeCandidat) != null)
            return candidatRepository.findById(codeCandidat).orElse(null);
        else
            return null;
    }

    public ResponseEntity<String> addCandidat(Candidat candidat) {
        try {
            candidatRepository.save(candidat);
            return new ResponseEntity<>("Etudiant cree avec succes", HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>("Erreur dans la creation du candidat " + e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    public String updateCandidat(Candidat candidat) {
        try {
            candidatRepository.save(candidat);
            return "Mise a jour reussie";
        } catch (Exception e) {
            return "Erreur dans la mise a jour du candidat " + e.getMessage();
        }
    }

    public String deleteCandidat(Long codeCandidat) {
        try {
            Candidat candidat1 = candidatRepository.findById(codeCandidat).orElse(null);
            candidatRepository.delete(candidat1);
            return "Suppression reussie";
        } catch (Exception e) {
            return "Erreur dans la suppression du candidat " + e.getMessage();
        }
    }
}
