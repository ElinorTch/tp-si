package com.example.notification.controller;

import com.example.notification.models.Mail;
import com.example.notification.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/notification")
public class NotificationController {

    @Autowired
    EmailService emailService;

    @PostMapping()
    public ResponseEntity<String> sendEmail(
            @RequestBody Mail mail
    ) {
        return emailService.sendEmail(mail);
    }
}
