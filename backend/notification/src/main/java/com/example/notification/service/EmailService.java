package com.example.notification.service;

import com.example.notification.models.Mail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.net.InetAddress;
import java.net.UnknownHostException;

@Service
public class EmailService {

    @Autowired
    private JavaMailSender mailSender;

    public ResponseEntity<String> sendEmail(Mail mail) {
        try {
            SimpleMailMessage message = new SimpleMailMessage();

            String email = "tchamoelii@gmail.com";
            message.setFrom(email);
            message.setTo(mail.getToEmail());
            message.setText(mail.getBody());
            message.setSubject(mail.getSubject());

            mailSender.send(message);
            System.out.println("Mail sent successfully...");
            return new ResponseEntity<>("Mail sent successfully...", HttpStatus.OK);
        } catch (Exception e) {
            System.out.println("Erreur dans la l'envoie de l'email : " + e.getMessage());
            return new ResponseEntity<>("Erreur dans la l'envoie de l'email : " + e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
