package com.example.userinterface.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class NiveauDto {
    private Long codeNiveau;
    private String descriptionNiveau;
    private String libelleNiveau;

    private Integer numero;
}
