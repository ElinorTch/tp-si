package com.example.userinterface.service;

import com.example.userinterface.dto.InscriptionDto;
import com.example.userinterface.utils.URL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class InscriptionService {

    @Autowired
    EtudiantService etudiantService;

    public ResponseEntity<List<InscriptionDto>> getAllInscription() {
        RestTemplate restTemplate = new RestTemplate();
        ParameterizedTypeReference<List<InscriptionDto>> responseType = new ParameterizedTypeReference<List<InscriptionDto>>() {};
        ResponseEntity<List<InscriptionDto>> responseEntity = restTemplate.exchange(
                URL.BASE_URL_INSCRIPTION,
                HttpMethod.GET,
                null,
                responseType
        );
        return new ResponseEntity<>(responseEntity.getBody(), HttpStatus.OK);
    }

    public ResponseEntity<String> createInscription(InscriptionDto inscriptionDto) throws Exception {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<InscriptionDto> inscriptionDtoHttpEntity = new HttpEntity<>(inscriptionDto, headers);

        ResponseEntity<String> inscriptionResponse = restTemplate.exchange(
                URL.BASE_URL_INSCRIPTION + "/createInscription",
                HttpMethod.POST,
                inscriptionDtoHttpEntity,
                String.class
        );
        return new ResponseEntity<>(inscriptionResponse.getBody(), HttpStatus.CREATED);
    }

    public String updateInscription(InscriptionDto inscriptionDto) throws Exception {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<InscriptionDto> inscriptionDtoHttpEntity = new HttpEntity<>(inscriptionDto, headers);

        ResponseEntity<String> inscriptionResponse = restTemplate.exchange(
                URL.BASE_URL_INSCRIPTION + "/updateInscription",
                HttpMethod.POST,
                inscriptionDtoHttpEntity,
                String.class
        );
        return inscriptionResponse.getBody();
    }
}
