package com.example.userinterface.service;

import com.example.userinterface.dto.EtudiantDto;
import com.example.userinterface.dto.InscriptionDto;
import com.example.userinterface.dto.MailDto;
import com.example.userinterface.utils.URL;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class MailDtoService {

    public String sendNotif(InscriptionDto inscriptionDto, EtudiantDto etudiantDto) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        MailDto mailDto = new MailDto(
                etudiantDto.getEmail(),
                "Versement pension IUSJ",
                etudiantDto.getLastname() +  etudiantDto.getFirstname() + ": vous avez bien ete inscrit, montant: " + inscriptionDto.getMontant() +
                        ". Statut du paiement: " + inscriptionDto.getStatut()
        );
        HttpEntity<MailDto> mailDtoHttpEntity = new HttpEntity<>(mailDto, headers);
        ResponseEntity<String> mailDtoResponse = restTemplate.exchange(
                URL.BASE_URL_NOTIFICATION,
                HttpMethod.POST,
                mailDtoHttpEntity,
                String.class
        );
        System.out.println(mailDtoResponse.getBody());
        return mailDtoResponse.getBody();
    }
}
