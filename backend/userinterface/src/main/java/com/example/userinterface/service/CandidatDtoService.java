package com.example.userinterface.service;

import com.example.userinterface.dto.CandidatDto;
import com.example.userinterface.utils.URL;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class CandidatDtoService {

    public List<CandidatDto> getAllCandidat() {
        RestTemplate restTemplate = new RestTemplate();
        ParameterizedTypeReference<List<CandidatDto>> responseType = new ParameterizedTypeReference<List<CandidatDto>>() {};
        ResponseEntity<List<CandidatDto>> etudiantDto = restTemplate.exchange(
                URL.BASE_URL_CANDIDAT,
                HttpMethod.GET,
                null,
                responseType
        );
        return etudiantDto.getBody();
    }

    public CandidatDto getCandidatByCode(Long codeCandidat) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<CandidatDto> candidatDtoResponseEntity = restTemplate.exchange(
                URL.BASE_URL_CANDIDAT + "/{codeCandidat}",
                HttpMethod.GET,
                null,
                CandidatDto.class,
                codeCandidat
        );

        return candidatDtoResponseEntity.getBody();
    }

    public ResponseEntity<String> createCandidatDto(CandidatDto candidatDto) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        System.out.println(candidatDto);

        HttpEntity<CandidatDto> candidatDtoHttpEntity = new HttpEntity<>(candidatDto, headers);

        ResponseEntity<String> candidatDtoResponseEntity = restTemplate.exchange(
                URL.BASE_URL_CANDIDAT + "/createCandidat",
                HttpMethod.POST,
                candidatDtoHttpEntity,
                String.class
        );

        return new ResponseEntity<>(candidatDtoResponseEntity.getBody(), HttpStatus.CREATED);
    }

    public String updateCandidat(CandidatDto candidatDto) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<CandidatDto> candidatDtoHttpEntity = new HttpEntity<>(candidatDto, headers);

        ResponseEntity<String> candidatDtoResponseEntity = restTemplate.exchange(
                URL.BASE_URL_CANDIDAT + "/updateCandidat",
                HttpMethod.POST,
                candidatDtoHttpEntity,
                String.class
        );

        return candidatDtoResponseEntity.getBody();
    }

}
