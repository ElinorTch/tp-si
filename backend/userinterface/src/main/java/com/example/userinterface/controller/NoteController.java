package com.example.userinterface.controller;

import com.example.userinterface.dto.NoteDto;
import com.example.userinterface.service.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/composite")
public class NoteController {

    @Autowired
    NoteService noteService;

    @PostMapping("/note/{codeMatiere}")
    public ResponseEntity<String> createNote(
            @RequestBody NoteDto noteDto,
            @PathVariable("codeMatiere") Long codeMatiere
    ) {
        return noteService.createNote(noteDto, codeMatiere);
    }
}
