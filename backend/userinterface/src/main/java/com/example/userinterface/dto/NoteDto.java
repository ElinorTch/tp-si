package com.example.userinterface.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class NoteDto {
    private long codeNote;

    private Long codeEtudiant;

    private Double valeurNote;

    private Date dateEnregistrement;

}
