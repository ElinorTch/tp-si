package com.example.userinterface.controller;

import com.example.userinterface.dto.CandidatDto;
import com.example.userinterface.dto.EtudiantDto;
import com.example.userinterface.service.EtudiantService;
import com.netflix.discovery.converters.Auto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("http://localhost:4200")
@RequestMapping("api/composite")
public class EtudiantController {

    @Autowired
    EtudiantService etudiantService;

    @GetMapping("/etudiant")
    public List<CandidatDto> getAllCandidat() {
        return etudiantService.getAllEtudiant();
    }

    @PostMapping("/login")
    public ResponseEntity login(
            @RequestBody CandidatDto candidatDto
    ) {
        return etudiantService.login(candidatDto);
    }
}
