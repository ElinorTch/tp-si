package com.example.userinterface.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class InscriptionDto {
    private Long codeInscription;

    private Long codeEtudiant;

    private String statut;

    private Long montant;

    private Date dateInscription;
    private Date dateModification;
}
