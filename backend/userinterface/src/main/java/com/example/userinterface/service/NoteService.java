package com.example.userinterface.service;

import com.example.userinterface.dto.EtudiantDto;
import com.example.userinterface.dto.InscriptionDto;
import com.example.userinterface.dto.NoteDto;
import com.example.userinterface.utils.URL;
import com.netflix.discovery.converters.Auto;
import org.aspectj.weaver.ast.Not;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class NoteService {

    @Autowired
    EtudiantService etudiantService;

    public ResponseEntity<String> createNote(NoteDto noteDto, Long codeMatiere) {
        RestTemplate restTemplate = new RestTemplate();

        if (!etudiantService.etudiantExist(noteDto.getCodeEtudiant())) {
            System.out.println("L'etudiant n'existe pas");
            return new ResponseEntity<>("L'etudiant n'existe pas", HttpStatus.BAD_REQUEST);
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<NoteDto> noteDtoHttpEntity = new HttpEntity<>(noteDto, headers);

        ResponseEntity<String> noteResponse = restTemplate.exchange(
                URL.BASE_URL_NOTE + "/{codeMatiere}",
                HttpMethod.POST,
                noteDtoHttpEntity,
                String.class,
                codeMatiere
        );
        return new ResponseEntity<>(noteResponse.getBody(), HttpStatus.CREATED);
    }
}
