package com.example.userinterface.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CandidatDto {
    private Long codeCandidat;

    private Long telephone;

    private String sex;
    private String email;
    private String lastname;
    private String firstname;

    private Date dateCreation;
    private Date dateModification;
    private Date birthdate;

    private Boolean statut;
    private String password;

    private String role;
}
