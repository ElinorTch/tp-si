package com.example.userinterface.controller;

import com.example.userinterface.dto.CandidatDto;
import com.example.userinterface.service.CandidatDtoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/composite")
@CrossOrigin("http://localhost:4200")
public class CandidatController {

    @Autowired
    CandidatDtoService candidatDtoService;

    @GetMapping("/candidats")
    public List<CandidatDto> getAllCandidat() {
        return candidatDtoService.getAllCandidat();
    }

    @GetMapping("/candidat/{codeCandidat}")
    public CandidatDto getCandidatByCode(
            @PathVariable("codeCandidat") Long codeCandidat
    ) {
        return candidatDtoService.getCandidatByCode(codeCandidat);
    }

    @PostMapping("/candidat")
    public ResponseEntity<String> createCandidat(
            @RequestBody CandidatDto candidatDto
    ) {
        return candidatDtoService.createCandidatDto(candidatDto);
    }

    @PutMapping("/candidat")
    public String updateCandidat(
            @RequestBody CandidatDto candidatDto
    ) {
        return candidatDtoService.updateCandidat(candidatDto);
    }
}
