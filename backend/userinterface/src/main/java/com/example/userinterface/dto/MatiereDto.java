package com.example.userinterface.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class MatiereDto {
    private long codeMatiere;

    private String libelleMatiere;
}
