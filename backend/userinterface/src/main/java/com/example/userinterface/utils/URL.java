package com.example.userinterface.utils;

public class URL {
    public static final String BASE_URL_CANDIDAT = "http://localhost:8010/api/candidat";
    public static final String BASE_URL_INSCRIPTION = "http://localhost:8011/api/inscription";
    public static final String BASE_URL_NOTE = "http://localhost:8012/api/note";
}
