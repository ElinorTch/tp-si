package com.example.userinterface.controller;

import com.example.userinterface.dto.InscriptionDto;
import com.example.userinterface.service.InscriptionService;
import com.netflix.discovery.converters.Auto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/composite")
public class InscriptionController {

    @Autowired
    InscriptionService inscriptionService;

    @GetMapping("/inscription")
    public ResponseEntity<List<InscriptionDto>> getAllInscription() {
        return inscriptionService.getAllInscription();
    }

    @PostMapping("/inscription")
    public ResponseEntity<String> createInscription(
            @RequestBody InscriptionDto inscriptionDto
    ) throws Exception {
        return inscriptionService.createInscription(inscriptionDto);
    }

    @PutMapping("/inscription")
    public String updateInscription(
            @RequestBody InscriptionDto inscriptionDto
    ) throws Exception {
        return inscriptionService.updateInscription(inscriptionDto);
    }
}
