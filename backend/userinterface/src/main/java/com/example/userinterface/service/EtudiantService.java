package com.example.userinterface.service;

import com.example.userinterface.dto.CandidatDto;
import com.example.userinterface.dto.EtudiantDto;
import com.example.userinterface.dto.InscriptionDto;
import com.example.userinterface.utils.URL;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class EtudiantService {

    public boolean etudiantExist(Long codeEtudiant) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<EtudiantDto> etudiantDto = restTemplate.exchange(
                URL.BASE_URL_CANDIDAT + "/{codeEtudiant}",
                HttpMethod.GET,
                null,
                EtudiantDto.class,
                codeEtudiant
        );

        if (etudiantDto.getBody() == null) {
            System.out.println("L'etudiant n'existe pas");
            return false;
        }

        System.out.println(etudiantDto.getBody());
        return true;
    }

    public List<CandidatDto> getAllEtudiant() {
        RestTemplate restTemplate = new RestTemplate();
        ParameterizedTypeReference<List<CandidatDto>> responseTypeCandidat = new ParameterizedTypeReference<>() {
        };
        ResponseEntity<List<CandidatDto>> candidatDto = restTemplate.exchange(
                URL.BASE_URL_CANDIDAT + "/get",
                HttpMethod.GET,
                null,
                responseTypeCandidat
        );

        ParameterizedTypeReference<List<InscriptionDto>> responseType = new ParameterizedTypeReference<>() {
        };
        ResponseEntity<List<InscriptionDto>> inscriptionDto = restTemplate.exchange(
                URL.BASE_URL_INSCRIPTION,
                HttpMethod.GET,
                null,
                responseType
        );

        List<CandidatDto> etudiantInscrit = new ArrayList<>();
        for (InscriptionDto inscriptionDto1 : Objects.requireNonNull(inscriptionDto.getBody())) {
            etudiantInscrit.add(Objects.requireNonNull(candidatDto.getBody()).stream().filter(
                    etudiant -> Objects.equals(etudiant.getCodeCandidat(), inscriptionDto1.getCodeEtudiant())
            ).findFirst().orElse(null));
        }

        System.out.println(etudiantInscrit);
        return etudiantInscrit;
    }

    public ResponseEntity login(CandidatDto candidatDto) {
        CandidatDto candidatDtos = getAllEtudiant().stream().filter(
                candidatDto1 -> Objects.equals(candidatDto1.getCodeCandidat(), candidatDto.getCodeCandidat())
        ).findFirst().orElse(null);

        System.out.println(candidatDtos);
        assert candidatDtos != null;
        if (Objects.equals(candidatDtos.getPassword(), candidatDto.getPassword()))
            return new ResponseEntity<>(candidatDtos, HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
