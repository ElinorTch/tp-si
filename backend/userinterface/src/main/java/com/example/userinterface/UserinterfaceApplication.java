package com.example.userinterface;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class UserinterfaceApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserinterfaceApplication.class, args);
	}

}
