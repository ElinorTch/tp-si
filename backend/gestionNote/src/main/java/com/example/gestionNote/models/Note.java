package com.example.gestionNote.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Note {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long codeNote;

    private Long codeEtudiant;

    private Double valeurNote;

    private Date dateEnregistrement;

    @ManyToOne
    @JoinColumn(name = "codeMatiere")
    private Matiere matiere;
}
