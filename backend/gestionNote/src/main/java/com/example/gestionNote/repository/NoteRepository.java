package com.example.gestionNote.repository;

import com.example.gestionNote.models.Note;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RestController;

@Repository
public interface NoteRepository extends JpaRepository<Note, Long> {
}
