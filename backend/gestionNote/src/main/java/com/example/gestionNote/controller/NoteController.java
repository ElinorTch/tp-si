package com.example.gestionNote.controller;

import com.example.gestionNote.models.Note;
import com.example.gestionNote.service.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("http://localhost:4200")
@RequestMapping("api/note")
public class NoteController {

    @Autowired
    NoteService noteService;

    @GetMapping()
    public ResponseEntity<List<Note>> getAllNote() {
        return noteService.getAllNote();
    }

    @GetMapping("/{codeEtudiant}")
    public ResponseEntity<List<Note>> getNoteByEtudiant(
            @PathVariable("codeEtudiant") Long codeEtudiant
    ) {
        return noteService.getNoteByEtudiant(codeEtudiant);
    }

    @PostMapping("/{codeMatiere}")
    public ResponseEntity<String> createNote(
            @PathVariable("codeMatiere") Long codeMatiere,
            @RequestBody Note note
    ) {
        return noteService.createNote(codeMatiere, note);
    }

    @PutMapping()
    public ResponseEntity<String> createNote(
            @RequestBody Note note
    ) {
        return noteService.updateNote(note);
    }
}
