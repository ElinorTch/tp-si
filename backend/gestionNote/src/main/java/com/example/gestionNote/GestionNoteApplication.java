package com.example.gestionNote;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class GestionNoteApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestionNoteApplication.class, args);
	}

}
