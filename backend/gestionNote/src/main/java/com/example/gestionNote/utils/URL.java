package com.example.gestionNote.utils;

public class URL {
    public static final String BASE_URL_CANDIDAT = "http://localhost:8010/api/candidat";
    public static final String BASE_URL_INSCRIPTION = "http://localhost:8010/api/inscription";
    public static final String BASE_URL_NOTE = "http://localhost:8010/api/note";
}
