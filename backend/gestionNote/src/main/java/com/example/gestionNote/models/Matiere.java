package com.example.gestionNote.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Matiere {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long codeMatiere;

    private String libelleMatiere;

    @JsonIgnore
    @ToString.Exclude
    @OneToMany(mappedBy = "matiere")
    private List<Note> note;

    @ManyToOne
    @JoinColumn(name = "codeNiveau")
    private Niveau niveau;
}
