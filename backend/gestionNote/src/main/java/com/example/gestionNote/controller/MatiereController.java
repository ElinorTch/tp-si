package com.example.gestionNote.controller;

import com.example.gestionNote.models.Matiere;
import com.example.gestionNote.service.MatiereService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin("http://localhost:4200")
@RequestMapping("api/note")
public class MatiereController {

    @Autowired
    MatiereService matiereService;

    @GetMapping("/matiere")
    public List<Matiere> getAllMatiere() {
        return matiereService.getAllMatiere();
    }
}
