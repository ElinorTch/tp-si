package com.example.gestionNote.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Niveau {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long codeNiveau;

    private String descriptionNiveau;
    private String libelleNiveau;

    private Integer numero;

    @JsonIgnore
    @ToString.Exclude
    @OneToMany(mappedBy = "niveau")
    private List<Matiere> matiere;
}
