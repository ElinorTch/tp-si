package com.example.gestionNote.service;

import com.example.gestionNote.models.Matiere;
import com.example.gestionNote.models.Note;
import com.example.gestionNote.repository.MatiereRepository;
import com.example.gestionNote.repository.NoteRepository;
import com.example.gestionNote.utils.URL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class NoteService {

    @Autowired
    NoteRepository noteRepository;

    @Autowired
    MatiereRepository matiereRepository;

    public ResponseEntity<List<Note>> getAllNote() {
        return new ResponseEntity<>(noteRepository.findAll(), HttpStatus.OK);
    }

    public ResponseEntity<List<Note>> getNoteByEtudiant(Long codeEtudiant) {
        return new ResponseEntity<>(Objects.requireNonNull(getAllNote().getBody()).stream().filter(
                note -> Objects.equals(note.getCodeEtudiant(), codeEtudiant)
        ).toList(), HttpStatus.OK);
    }

    public ResponseEntity<String> createNote(Long codeMatiere, Note note) {
        Matiere matiere = new Matiere();
        matiere = matiereRepository.findById(codeMatiere).orElse(null);

        note.setMatiere(matiere);
        note.setDateEnregistrement(new Date());
        System.out.println(note);

        noteRepository.save(note);

        return new ResponseEntity<>("Note enregistree", HttpStatus.CREATED);
    }

    public ResponseEntity<String> updateNote(Note note) {
        System.out.println(note);

        noteRepository.save(note);

        return new ResponseEntity<>("Note enregistree", HttpStatus.CREATED);
    }


}
