package com.example.gestionNote.service;

import com.example.gestionNote.models.Matiere;
import com.example.gestionNote.repository.MatiereRepository;
import com.netflix.discovery.converters.Auto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MatiereService {

    @Autowired
    MatiereRepository matiereRepository;

    public List<Matiere> getAllMatiere() {
        return matiereRepository.findAll();
    }
}
