package com.example.gestioninscription.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Inscription {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long codeInscription;

    private Long codeEtudiant;

    private String statut;

    private Long montant;

    private Date dateInscription;
    private Date dateModification;

    private String matriculeEtudiant;
}
