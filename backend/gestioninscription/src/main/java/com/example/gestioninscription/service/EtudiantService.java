package com.example.gestioninscription.service;

import com.example.gestioninscription.models.Etudiant;
import com.example.gestioninscription.repository.EtudiantRepository;
import com.example.userinterface.dto.CandidatDto;
import com.example.userinterface.dto.EtudiantDto;
import com.example.userinterface.utils.URL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class EtudiantService {

    @Autowired
    EtudiantRepository etudiantRepository;

    public Etudiant etudiantExist(Long codeEtudiant) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Etudiant> etudiant = restTemplate.exchange(
                URL.BASE_URL_CANDIDAT + "/{codeEtudiant}",
                HttpMethod.GET,
                null,
                Etudiant.class,
                codeEtudiant
        );

        if (etudiant.getBody() == null) {
            System.out.println("L'etudiant n'existe pas");
            return null;
        }

        System.out.println("Etudiant : "+etudiant.getBody());
        return etudiant.getBody();
    }

    public List<Etudiant> getAllEtudiant() {
        return etudiantRepository.findAll();
    }

    public Etudiant getEtudiantById(Long code) {
        return etudiantRepository.findById(code).orElse(null);
    }

    public void saveEtudiant(Etudiant etudiant) {
        etudiantRepository.save(etudiant);
    }
}
