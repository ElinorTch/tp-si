package com.example.gestioninscription.controller;

import com.example.gestioninscription.models.Etudiant;
import com.example.gestioninscription.service.EtudiantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("http://localhost:4200")
@RequestMapping("api/inscription")
public class EtudiantController {

    @Autowired
    EtudiantService etudiantService;

    @GetMapping("/etudiant")
    public List<Etudiant> getAllEtudiant() {
        return etudiantService.getAllEtudiant();
    }

    @GetMapping("/etudiant/{codeEtudiant}")
    public Etudiant getEtudiantByCode(
            @PathVariable("codeEtudiant") Long codeEtudiant
    ) {
        return etudiantService.getEtudiantById(codeEtudiant);
    }
}
