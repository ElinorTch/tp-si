package com.example.gestioninscription.service;

import com.example.gestioninscription.models.Etudiant;
import com.example.gestioninscription.models.Inscription;
import com.example.gestioninscription.repository.InscriptionRepository;
import com.example.userinterface.utils.URL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class InscriptionService {

    @Autowired
    InscriptionRepository inscriptionRepository;

    @Autowired
    EtudiantService etudiantService;

    private void saveIntervention(Inscription inscription) {
        inscriptionRepository.save(inscription);
    }

    public List<Inscription> getAllInscription() {
        return inscriptionRepository.findAll();
    }

    public List<Inscription> getInscriptionByEtudiant(Long codeEtudiant) {
        List<Inscription> inscriptionList = getAllInscription();
        return inscriptionList.stream()
                .filter(inscription -> Objects.equals(inscription.getCodeEtudiant(), codeEtudiant))
                .toList();
    }

    public ResponseEntity<String> createInscription(Inscription inscription) {
        List<Inscription> listInscription = this.getAllInscription().stream()
                .filter(inscription1 -> Objects.equals(inscription1.getCodeEtudiant(), inscription.getCodeEtudiant())).toList();

        Etudiant etudiant = etudiantService.etudiantExist(inscription.getCodeEtudiant());

        if (etudiant == null) {
            return new ResponseEntity<>("Inscription echoue, Etudiant n'existe pas", HttpStatus.BAD_REQUEST);
        }

        if (listInscription.size() != 0) {
//            throw new Exception();
            System.out.println("Etudiant deja inscrit");
             return new ResponseEntity<>("Inscription echoue, Etudiant deja inscrit", HttpStatus.BAD_REQUEST);
        }

        etudiantService.saveEtudiant(etudiant);

        inscription.setDateInscription(new Date());
        inscription.setDateModification(new Date());

        if (inscription.getMontant() >= 1600000)
            inscription.setStatut("PAYEE");
        else if (inscription.getMontant() > 0 && inscription.getMontant() < 1600000)
            inscription.setStatut("EN COURS");

        this.saveIntervention(inscription);
        return new ResponseEntity<>("Inscription reussie", HttpStatus.CREATED);
    }

    public ResponseEntity<String> updateInscription(Inscription inscription1) {
        Inscription inscription = inscriptionRepository.findById(inscription1.getCodeInscription()).orElse(null);
        inscription.setDateModification(new Date());

        if ((inscription.getMontant() + inscription1.getMontant()) >= 1600000) {
            inscription.setMontant(inscription.getMontant() + inscription1.getMontant());
            inscription.setStatut("PAYEE");
        } else if (inscription.getMontant() > 0 && inscription.getMontant() < 1600000) {
            inscription.setMontant(inscription.getMontant() + inscription1.getMontant());
            inscription.setStatut("EN COURS");
        }

        this.saveIntervention(inscription);
        return new ResponseEntity<>("Update reussie", HttpStatus.CREATED);
    }
}
