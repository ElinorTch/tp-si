package com.example.gestioninscription.controller;

import com.example.gestioninscription.models.Inscription;
import com.example.gestioninscription.service.InscriptionService;
import com.netflix.discovery.converters.Auto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("http://localhost:4200")
@RequestMapping("api/inscription")
public class InscriptionController {

    @Autowired
    InscriptionService inscriptionService;

    @GetMapping()
    public List<Inscription> getAllInscription() {
        return inscriptionService.getAllInscription();
    }

    @GetMapping("/{codeEtudiant}")
    public List<Inscription> getInscriptionByEtudiant(
            @PathVariable("codeEtudiant") Long codeEtudiant
    ) {
        return inscriptionService.getInscriptionByEtudiant(codeEtudiant);
    }

    @PostMapping("/createInscription")
    public ResponseEntity<String> createInscription(
            @RequestBody Inscription inscription
    ) {
        return inscriptionService.createInscription(inscription);
    }

    @PutMapping("/updateInscription")
    public ResponseEntity<String> updateInscription(
            @RequestBody Inscription inscription
    ) {
        return inscriptionService.updateInscription(inscription);
    }
}