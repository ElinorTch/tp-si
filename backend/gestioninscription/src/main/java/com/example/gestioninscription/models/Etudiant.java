package com.example.gestioninscription.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Etudiant{
    @Id
    @Column(name = "code")
    private Long codeCandidat;

    @Column(name = "nom")
    private String firstname;

    @Column(name = "prenom")
    private String lastname;
}
