export const environment = {
  production: false,
  URL_COMPOSITE: 'http://localhost:8020/userinterface/api/composite',
  URL_INSCRIPTION: 'http://localhost:8020/gestioninscription/api/inscription',
  URL_NOTE: 'http://localhost:8020/gestionnote/api/note',
  URL_ETUDIANT: 'http://localhost:8020/gestionetudiant/api/candidat'
};
