import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SidebarModule } from 'primeng/sidebar';
import { ButtonModule } from 'primeng/button';
import { MenubarModule } from 'primeng/menubar';
import { AvatarModule } from 'primeng/avatar';
import { BadgeModule } from 'primeng/badge';
import { TableModule } from 'primeng/table';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { ToastModule } from 'primeng/toast';
import { DialogService } from 'primeng/dynamicdialog';
import { MessageService } from 'primeng/api';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { PersonnelComponent } from './modules/authentication/personnel/personnel.component';
import { EtudiantComponent } from './modules/authentication/etudiant/etudiant.component';

@NgModule({
  declarations: [
    AppComponent,
    PersonnelComponent,
    EtudiantComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SidebarModule,
    ButtonModule,
    MenubarModule,
    AvatarModule,
    BadgeModule,
    TableModule,
    HttpClientModule,
    FormsModule,
    InputTextModule,
    DynamicDialogModule,
    ToastModule,
    ReactiveFormsModule
  ],
  providers: [DialogService, MessageService],
  bootstrap: [AppComponent],
  schemas: [],
})
export class AppModule { }
