import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EtudiantComponent } from './etudiant/etudiant.component';
import { PersonnelComponent } from './personnel/personnel.component';

const routes: Routes = [
  { path: 'etudiant-login', component: EtudiantComponent},
  { path: 'admin-login', component: PersonnelComponent },
  { path: '',
    redirectTo: 'etudiant-login',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthenticationRoutingModule { }
