import { Component } from '@angular/core';
import { FormControl, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { EtudiantAuthService } from 'src/app/services/etudiant-auth.service';

@Component({
  selector: 'app-etudiant',
  templateUrl: './etudiant.component.html',
  styleUrls: ['./etudiant.component.scss']
})
export class EtudiantComponent {


  submitting = false;
  loginForm = this.formBuilder.group({
    codeCandidat: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required])
  });
  constructor(private router: Router, private formBuilder: FormBuilder, private messageService: MessageService, private authService: EtudiantAuthService) { }


  login(): void {
    this.submitting = true;
    console.log(this.loginForm.value);

    //appel de la requete du service
    this.authService.login(this.loginForm.value).subscribe(
      (data: any) => {
        this.submitting = false;
        this.messageService.add({
          severity: 'success',
          summary: 'succès',
          detail: 'Vous êtes connecté',
          life: 3000
        });
      console.log(data);
      localStorage.setItem("user", JSON.stringify(data));
      this.router.navigate(['/app/note-list'])
    },
      (res) => {
        this.submitting = false;
        this.messageService.add({
          severity: 'error',
          summary: ' Erreur',
          detail: 'Vérifier votre login ou mot de passe',
          life: 3000
        });
        console.log(res);

        this.loginForm.value.codeCandidat = "";
        this.loginForm.value.password = "";
      });
  }

  formPersonnel() {
    this.router.navigate(['auth/admin-login'])
  }



  ngOnInit(): void {

  }

}
