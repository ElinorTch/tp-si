import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentsRoutingModule } from './components-routing.module';
import { ComponentsComponent } from './components.component';
import { HeaderComponent } from '../shared/components/header/header.component';
import { SidebarComponent } from '../shared/components/sidebar/sidebar.component';
import { FormCandidatComponent } from './form/form-candidat/form-candidat.component';
import { FormUpdateComponent } from './form/form-candidat/form-update/form-update.component';
import { FormEtudiantComponent } from './form/form-etudiant/form-etudiant.component';
import { FormInscriptionUpdateComponent } from './form/form-inscription/form-inscription-update/form-inscription-update.component';
import { FormInscriptionComponent } from './form/form-inscription/form-inscription.component';
import { FormNoteUpdateComponent } from './form/form-note/form-note-update/form-note-update.component';
import { FormNoteComponent } from './form/form-note/form-note.component';
import { CandidatItemComponent } from './listes/candidat-list/candidat-item/candidat-item.component';
import { CandidatListComponent } from './listes/candidat-list/candidat-list.component';
import { EtudiantItemComponent } from './listes/etudiants-list/etudiant-item/etudiant-item.component';
import { EtudiantsListComponent } from './listes/etudiants-list/etudiants-list.component';
import { InscriptionItemComponent } from './listes/inscription-list/inscription-item/inscription-item.component';
import { InscriptionListComponent } from './listes/inscription-list/inscription-list.component';
import { NoteListComponent } from './listes/note-list/note-list.component';
import { NotesItemComponent } from './listes/note-list/notes-item/notes-item.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AvatarModule } from 'primeng/avatar';
import { BadgeModule } from 'primeng/badge';
import { ButtonModule } from 'primeng/button';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { InputTextModule } from 'primeng/inputtext';
import { MenubarModule } from 'primeng/menubar';
import { SidebarModule } from 'primeng/sidebar';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';


@NgModule({
  declarations: [
    ComponentsComponent,
    SidebarComponent,
    HeaderComponent,
    EtudiantsListComponent,
    EtudiantItemComponent,
    CandidatListComponent,
    CandidatItemComponent,
    FormCandidatComponent,
    FormEtudiantComponent,
    FormInscriptionComponent,
    InscriptionListComponent,
    InscriptionItemComponent,
    FormUpdateComponent,
    FormInscriptionUpdateComponent,
    NoteListComponent,
    NotesItemComponent,
    FormNoteComponent,
    FormNoteUpdateComponent,
  ],
  imports: [
    CommonModule,
    ComponentsRoutingModule,
    SidebarModule,
    ButtonModule,
    MenubarModule,
    AvatarModule,
    BadgeModule,
    TableModule,
    HttpClientModule,
    FormsModule,
    InputTextModule,
    DynamicDialogModule,
    ToastModule,
    ReactiveFormsModule
  ]
})
export class ComponentsModule { }
