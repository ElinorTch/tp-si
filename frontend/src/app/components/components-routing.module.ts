import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CandidatListComponent } from './listes/candidat-list/candidat-list.component';
import { EtudiantsListComponent } from './listes/etudiants-list/etudiants-list.component';
import { InscriptionListComponent } from './listes/inscription-list/inscription-list.component';
import { NoteListComponent } from './listes/note-list/note-list.component';

const routes: Routes = [
  { path: 'etudiant-list', component: EtudiantsListComponent, pathMatch: 'full' },
  { path: 'candidat-list', component: CandidatListComponent },
  { path: 'inscription-list', component: InscriptionListComponent },
  { path: 'note-list', component: NoteListComponent },
  {
    path: '',
    redirectTo: 'candidat-list',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComponentsRoutingModule { }
