import { Component } from '@angular/core';
import { DynamicDialogRef, DialogService } from 'primeng/dynamicdialog';
import { CandidatService } from 'src/app/services/candidat.service';
import { FormCandidatComponent } from '../../form/form-candidat/form-candidat.component';
import { FormUpdateComponent } from '../../form/form-candidat/form-update/form-update.component';

@Component({
  selector: 'app-candidat-list',
  templateUrl: './candidat-list.component.html',
  styleUrls: ['./candidat-list.component.scss']
})
export class CandidatListComponent {
  candidats : any;
  ref: DynamicDialogRef | undefined;
  loading = true;

  constructor(
    public dialogService: DialogService,
    private _candidatService: CandidatService
  ) { }

  ngOnInit(): void {
    this.getAllCandidat();
  }

  show() {
    this.ref = this.dialogService.open(FormCandidatComponent,
      {width: '45%', height: '600px', contentStyle: {"padding-left": "100px", "overflow": "auto"}});
  }

  getAllCandidat() {
    return this._candidatService.getAllCandidat().subscribe(
      (data) => {
        console.log(data);
        setTimeout(
          () => {
            this.candidats = data;
            this.loading = false;
          }, 500
        )
      }
    )
  }

  updateEtudiant() {

  }
}
