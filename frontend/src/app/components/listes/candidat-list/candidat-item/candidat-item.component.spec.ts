import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CandidatItemComponent } from './candidat-item.component';

describe('CandidatItemComponent', () => {
  let component: CandidatItemComponent;
  let fixture: ComponentFixture<CandidatItemComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CandidatItemComponent]
    });
    fixture = TestBed.createComponent(CandidatItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
