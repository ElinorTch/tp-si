import { Component } from '@angular/core';
import { DynamicDialogRef, DialogService } from 'primeng/dynamicdialog';
import { CandidatService } from 'src/app/services/candidat.service';
import { FormCandidatComponent } from '../../form/form-candidat/form-candidat.component';
import { FormEtudiantComponent } from '../../form/form-etudiant/form-etudiant.component';
import { EtudiantService } from 'src/app/services/etudiant.service';

@Component({
  selector: 'app-etudiants-list',
  templateUrl: './etudiants-list.component.html',
  styleUrls: ['./etudiants-list.component.scss']
})
export class EtudiantsListComponent {
  etudiants : any;
  ref: DynamicDialogRef | undefined;
  loading = true

  constructor(
    public dialogService: DialogService,
    private _etudiant: EtudiantService
  ) { }

  ngOnInit(): void {
    this.getAllEtudiant();
  }

  show() {
    this.ref = this.dialogService.open(FormEtudiantComponent,
      {width: "60%", height: '600px', contentStyle: {"padding-left": "100px", "overflow": "auto"}});
  }

  getAllEtudiant() {
    return this._etudiant.getAllEtudiant().subscribe(
      (data) => {
        console.log("Etudiant: ",data);
        setTimeout(
          () => {
            this.etudiants = data;
            this.loading = false;
          }, 500
        )
      }
    )
  }
}
