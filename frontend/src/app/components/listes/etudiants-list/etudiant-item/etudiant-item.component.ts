import { Component, Input } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { FormUpdateComponent } from 'src/app/components/form/form-candidat/form-update/form-update.component';

@Component({
  selector: 'app-etudiant-item',
  templateUrl: './etudiant-item.component.html',
  styleUrls: ['./etudiant-item.component.scss']
})
export class EtudiantItemComponent {
  @Input() candidatList: any;
  @Input() isGettingAll = true;
  search = '';
  ref: DynamicDialogRef | undefined;

  constructor(
    private formBuilder: FormBuilder,
    private dialogService: DialogService
  ) { }

  interventionForm = this.formBuilder.group({
    codeCandidat: new FormControl(''),
    lastname: new FormControl('', [Validators.required]),
    firstname: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required]),
    telephone: new FormControl('', [Validators.required]),
  })

  showUpdate(etudiant: any) {
    localStorage.setItem('etudiant', JSON.stringify(etudiant))
    this.ref = this.dialogService.open(FormUpdateComponent,
      {width: '45%', height: '600px', contentStyle: {"padding-left": "100px", "overflow": "auto"}});
  }
}
