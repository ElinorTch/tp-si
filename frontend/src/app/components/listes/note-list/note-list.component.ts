import { Component, OnInit } from '@angular/core';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { FormInscriptionComponent } from '../../form/form-inscription/form-inscription.component';
import { NoteService } from 'src/app/services/note.service';
import { FormNoteComponent } from '../../form/form-note/form-note.component';

@Component({
  selector: 'app-note-list',
  templateUrl: './note-list.component.html',
  styleUrls: ['./note-list.component.scss']
})
export class NoteListComponent implements OnInit{
  note: any;
  ref: DynamicDialogRef | undefined;
  user: any;
  loading = true;

  constructor(
    private dialogService: DialogService,
    private _note: NoteService
  ) {}

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem("user") || '');

    this.getAllNote()
  }

  show() {
    this.ref = this.dialogService.open(FormNoteComponent,
      {width: "40%", height: '400px', contentStyle: {"padding-left": "100px", "overflow": "auto"}});
  }

  getAllNote() {
    if (this.user.role == "admin") {
      this._note.getAllNote().subscribe(
        (data) => {
          console.log("Note: ",data);
          setTimeout(
          () => {
            this.note = data;
            this.loading = false;
          }, 500
        )
        }
      )
    }
    else {
      this._note.getNoteByEtudiant(this.user.codeCandidat).subscribe(
        (data) => {
          console.log("Inscription: ",data);
          setTimeout(
            () => {
              this.note = data;
              this.loading = false;
            }, 500
          )
        }
      )
    }

  }
}
