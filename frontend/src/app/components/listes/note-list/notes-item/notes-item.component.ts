import { Component, Input } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { DynamicDialogRef, DialogService } from 'primeng/dynamicdialog';
import { FormInscriptionUpdateComponent } from 'src/app/components/form/form-inscription/form-inscription-update/form-inscription-update.component';
import { FormNoteUpdateComponent } from 'src/app/components/form/form-note/form-note-update/form-note-update.component';
import { FormNoteComponent } from 'src/app/components/form/form-note/form-note.component';

@Component({
  selector: 'app-notes-item',
  templateUrl: './notes-item.component.html',
  styleUrls: ['./notes-item.component.scss']
})
export class NotesItemComponent {
  @Input() noteList: any;
  @Input() isGettingAll = true;
  search = '';
  user: any;
  ref: DynamicDialogRef | undefined;

  constructor(
    private formBuilder: FormBuilder,
    private dialogService: DialogService
  ) {
    this.user = JSON.parse(localStorage.getItem("user") || '');
  }

  interventionForm = this.formBuilder.group({
    codeCandidat: new FormControl(''),
    lastname: new FormControl('', [Validators.required]),
    firstname: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required]),
    telephone: new FormControl('', [Validators.required]),
  })

  showUpdate(note: any) {
    localStorage.setItem('note', JSON.stringify(note))
    this.ref = this.dialogService.open(FormNoteUpdateComponent,
      {width: '45%', height: '400px', contentStyle: {"padding-left": "100px", "overflow": "auto"}});
  }
}
