import { Component, Input } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { FormUpdateComponent } from 'src/app/components/form/form-candidat/form-update/form-update.component';
import { FormInscriptionUpdateComponent } from 'src/app/components/form/form-inscription/form-inscription-update/form-inscription-update.component';

@Component({
  selector: 'app-inscription-item',
  templateUrl: './inscription-item.component.html',
  styleUrls: ['./inscription-item.component.scss']
})
export class InscriptionItemComponent {
  @Input() inscriptionList: any;
  @Input() isGettingAll = true;
  search = '';
  user: any;
  ref: DynamicDialogRef | undefined;

  constructor(
    private formBuilder: FormBuilder,
    private dialogService: DialogService
  ) {
    this.user = JSON.parse(localStorage.getItem("user") || '');
  }

  interventionForm = this.formBuilder.group({
    codeCandidat: new FormControl(''),
    lastname: new FormControl('', [Validators.required]),
    firstname: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required]),
    telephone: new FormControl('', [Validators.required]),
  })

  showUpdate(inscription: any) {
    localStorage.setItem('inscription', JSON.stringify(inscription))
    this.ref = this.dialogService.open(FormInscriptionUpdateComponent,
      {width: '45%', height: '400px', contentStyle: {"padding-left": "100px", "overflow": "auto"}});
  }
}
