import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InscriptionItemComponent } from './inscription-item.component';

describe('InscriptionItemComponent', () => {
  let component: InscriptionItemComponent;
  let fixture: ComponentFixture<InscriptionItemComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [InscriptionItemComponent]
    });
    fixture = TestBed.createComponent(InscriptionItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
