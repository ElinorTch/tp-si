import { Component } from '@angular/core';
import { DynamicDialogRef, DialogService } from 'primeng/dynamicdialog';
import { EtudiantService } from 'src/app/services/etudiant.service';
import { FormEtudiantComponent } from '../../form/form-etudiant/form-etudiant.component';
import { InscriptionService } from 'src/app/services/inscription.service';
import { FormInscriptionComponent } from '../../form/form-inscription/form-inscription.component';

@Component({
  selector: 'app-inscription-list',
  templateUrl: './inscription-list.component.html',
  styleUrls: ['./inscription-list.component.scss']
})
export class InscriptionListComponent {
  inscription : any;
  user: any;
  ref: DynamicDialogRef | undefined;
  loading = true;

  constructor(
    public dialogService: DialogService,
    private _inscription: InscriptionService,
  ) { }

  ngOnInit(): void {
      this.user = JSON.parse(localStorage.getItem("user") || '');

    this.getAllInscription();
  }

  show() {
    this.ref = this.dialogService.open(FormInscriptionComponent,
      {width: "40%", height: '400px', contentStyle: {"padding-left": "100px", "overflow": "auto"}});
  }

  getAllInscription() {
    if (this.user.role == "admin") {
      return this._inscription.getAllInscription().subscribe(
        (data) => {
          console.log("Inscription: ",data);
          setTimeout(
            () => {
              this.inscription = data;
              this.loading = false;
            }, 500
          )
        }
      )
    }
    else {
      return this._inscription.getInscriptionByEtudiant(this.user.codeCandidat).subscribe(
        (data) => {
          console.log("Inscription: ",data);
          setTimeout(
            () => {
              this.inscription = data;
              this.loading = false;
            }, 500
          )
        }
      )
    }
  }
}
