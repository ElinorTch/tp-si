import { Component } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { DynamicDialogRef, DialogService } from 'primeng/dynamicdialog';
import { CandidatService } from 'src/app/services/candidat.service';

@Component({
  selector: 'app-form-candidat',
  templateUrl: './form-candidat.component.html',
  styleUrls: ['./form-candidat.component.scss']
})
export class FormCandidatComponent {
  lastname: string = "";
  firstname: string = "";
  sex: string = "masculin";
  telephone: number = 0;
  birthdate: string = "";
  email: string = "";
  ref: DynamicDialogRef | undefined;
  submitting = false

  candidatForm = this.formBuilder.group({
    codeCandidat: new FormControl(null),
    firstname: new FormControl('', [Validators.required]),
    lastname: new FormControl('', [Validators.required]),
    birthdate: new FormControl('', [Validators.required]),
    sex: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.email]),
    telephone: new FormControl('', [Validators.required]),
  })

  constructor(
    public dialogService: DialogService,
    private formBuilder: FormBuilder,
    private _candidatService: CandidatService
  ) { }

  ngOnInit(): void { }

  show() {
    this.ref = this.dialogService.open(FormCandidatComponent,
      { header: 'Créer un candidat', width: '70%', contentStyle: { "padding-left": "500px", "overflow": "auto" } }
    );
  }

  sendData() {
    console.log(this.candidatForm.value);
    this.submitting = true
    this._candidatService.createCandidat(this.candidatForm.value).subscribe(
      (data) => {
        console.log(data);
        setTimeout(
          () => {
            this.submitting = false
            window.location.reload()
          }, 1000
        )
      },
      (error) => {
        setTimeout(
          () => {
            this.submitting = false
            window.location.reload()
          }, 1000
        )
      }
    )
  }
}
