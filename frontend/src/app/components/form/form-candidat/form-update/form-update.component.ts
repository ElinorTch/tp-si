import { Component, Input } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { CandidatService } from 'src/app/services/candidat.service';

@Component({
  selector: 'app-form-update',
  templateUrl: './form-update.component.html',
  styleUrls: ['./form-update.component.scss']
})
export class FormUpdateComponent {
  codeCandidat!: number;
  lastname: string = "";
  firstname: string = "";
  sex: string = "masculin";
  telephone: number = 0;
  birthdate: string = "";
  email: string = "";
  ref: DynamicDialogRef | undefined;
  submitting = false;

  candidatUpdateForm = this.formBuilder.group({
    codeCandidat: new FormControl(''),
    firstname: new FormControl('', [Validators.required]),
    lastname: new FormControl('', [Validators.required]),
    birthdate: new FormControl('', [Validators.required]),
    sex: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required]),
    telephone: new FormControl('', [Validators.required]),
  })

  constructor(
    public dialogService: DialogService,
    private formBuilder: FormBuilder,
    private _candidatService: CandidatService
  ) { }

  ngOnInit() {
    this.getData();
  }

  getData() {
    let etudiant = JSON.parse(localStorage.getItem("etudiant") || '');
    this.codeCandidat = etudiant.codeCandidat;
    this.lastname = etudiant.lastname;
    this.firstname = etudiant.firstname;
    this.sex = etudiant.sex;
    this.telephone = etudiant.telephone;
    this.birthdate = etudiant.birthdate;
    this.email = etudiant.email;
  }

  updateData() {
    console.log(this.candidatUpdateForm.value);
    this.submitting = true
    this._candidatService.updateCandidat(this.candidatUpdateForm.value).subscribe(
      (data) => {
        console.log(data);
        setTimeout(
          () => {
            this.submitting = false
            window.location.reload()
          }, 1000
        )
      },
      (error) => {
        setTimeout(
          () => {
            this.submitting = false
            window.location.reload()
          }, 1000
        )
      }
    )
  }
}
