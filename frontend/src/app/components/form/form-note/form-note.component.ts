import { Component } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { DynamicDialogRef, DialogService } from 'primeng/dynamicdialog';
import { CandidatService } from 'src/app/services/candidat.service';
import { InscriptionService } from 'src/app/services/inscription.service';
import { NoteService } from 'src/app/services/note.service';

@Component({
  selector: 'app-form-note',
  templateUrl: './form-note.component.html',
  styleUrls: ['./form-note.component.scss']
})
export class FormNoteComponent {
  candidats: any;
  matieres: any;
  valeurNote!: number;
  codeNote!: number;
  codeMatiere!: number;
  submitting = false
  ref: DynamicDialogRef | undefined;

  constructor(
    private formBuilder: FormBuilder,
    private _candidat: CandidatService,
    private _dialogService: DialogService,
    private _inscription: InscriptionService,
    private _note: NoteService,
    private messageService: MessageService
  ) { }

  ngOnInit() {
    this.getCandidat();
    this.getAllMatiere();
  }

  noteForm = this.formBuilder.group({
    codeNote: new FormControl(''),
    matiere: new FormControl('', [Validators.required]),
    codeEtudiant: new FormControl('', [Validators.required]),
    valeurNote: new FormControl('', [Validators.required])
  })

  getCandidat() {
    this._candidat.getAllCandidat().subscribe(
      (data) => {
        console.log('candidat: ', data);
        this.candidats = data;
      }
    )
  }

  getAllMatiere() {
    this._note.getAllMatiere().subscribe(
      (data) => {
        this.matieres = data
      }
    )
  }

  sendData() {
    this.submitting = true
    console.log(this.noteForm.value);
    let codeMatiere = parseInt(this.noteForm.value.matiere || '0')
    this._note.createNote(codeMatiere,
      { "codeEtudiant": this.noteForm.value.codeEtudiant, "valeurNote": this.noteForm.value.valeurNote }).subscribe(
        (data) => {
          console.log("Note reussie", data);
          setTimeout(
            () => {
              this.submitting = false
              window.location.reload()
              this.messageService.add({
                severity: 'success',
                summary: 'Note enregistre',
                detail: 'Enregistrer',
                life: 3000
              });
            }, 1000
          )
        },
        (error) => {
          console.log("erreur durant la MAJ de la note", error);
          setTimeout(
            () => {
              this.submitting = false
              window.location.reload()
              this.messageService.add({
                severity: 'error',
                summary: ' Erreur',
                detail: 'Verifier les infos',
                life: 3000
              });
            }, 1000
          )

        }
      )
  }
}
