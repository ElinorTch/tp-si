import { Component } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { DynamicDialogRef, DialogService } from 'primeng/dynamicdialog';
import { CandidatService } from 'src/app/services/candidat.service';
import { InscriptionService } from 'src/app/services/inscription.service';
import { NoteService } from 'src/app/services/note.service';

@Component({
  selector: 'app-form-note-update',
  templateUrl: './form-note-update.component.html',
  styleUrls: ['./form-note-update.component.scss']
})
export class FormNoteUpdateComponent {
  candidats: any;
  matieres: any;
  valeurNote: number = 0;
  codeNote: number = 0;
  codeMatiere: number = 0;
  note: any;
  submitting = false;
  ref: DynamicDialogRef | undefined;

  constructor(
    private formBuilder: FormBuilder,
    private _note: NoteService,
    private _candidat: CandidatService
  ) { }

  ngOnInit() {
    this.note = JSON.parse(localStorage.getItem("note") || '');
    console.log("Note", this.note);

    this.matieres = this.note.matiere;
    this.valeurNote = this.note.valeurNote;
    this.codeNote = this.note.codeNote;

    this._candidat.getCandidatById(this.note.codeEtudiant).subscribe(
      (data) => {
        console.log(data);
        this.candidats = data;
      }
    )
  }

  noteForm = this.formBuilder.group({
    codeNote: new FormControl(),
    matiere: new FormControl('', [Validators.required]),
    codeEtudiant: new FormControl('', [Validators.required]),
    valeurNote: new FormControl('', [Validators.required])
  })

  sendData() {
    console.log(this.noteForm.value);
    let codeMatiere = parseInt(this.noteForm.value.matiere || '0')
    this.submitting = true
    this._note.createNote(codeMatiere,
      { "codeNote":this.noteForm.value.codeNote, "codeEtudiant": this.noteForm.value.codeEtudiant, "valeurNote": this.noteForm.value.valeurNote }).subscribe(
        (data) => {
          console.log("Note reussie", data);
          setTimeout(
            () => {
              this.submitting = false
              window.location.reload()
            }, 1000
          )
        },
        (error) => {
          console.log("erreur durant la MAJ de la note", error);
          setTimeout(
            () => {
              this.submitting = false
              window.location.reload()
            }, 1000
          )
        }
      )
  }
}
