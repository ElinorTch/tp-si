import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormNoteUpdateComponent } from './form-note-update.component';

describe('FormNoteUpdateComponent', () => {
  let component: FormNoteUpdateComponent;
  let fixture: ComponentFixture<FormNoteUpdateComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FormNoteUpdateComponent]
    });
    fixture = TestBed.createComponent(FormNoteUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
