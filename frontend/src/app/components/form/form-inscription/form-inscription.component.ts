import { Component } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { CandidatService } from 'src/app/services/candidat.service';
import { InscriptionListComponent } from '../../listes/inscription-list/inscription-list.component';
import { InscriptionService } from 'src/app/services/inscription.service';
import { FormInscriptionUpdateComponent } from './form-inscription-update/form-inscription-update.component';

@Component({
  selector: 'app-form-inscription',
  templateUrl: './form-inscription.component.html',
  styleUrls: ['./form-inscription.component.scss']
})
export class FormInscriptionComponent {
  candidats: any;
  ref: DynamicDialogRef | undefined;
  submitting = false;

  constructor(
    private formBuilder: FormBuilder,
    private _candidat: CandidatService,
    private _dialogService: DialogService,
    private _inscription: InscriptionService
  ) { }

  ngOnInit() {
    this.getCandidat()
  }

  inscriptionForm = this.formBuilder.group({
    codeEtudiant: new FormControl('', [Validators.required]),
    montant: new FormControl('', [Validators.required])
  })

  getCandidat() {
    this._candidat.getAllCandidat().subscribe(
      (data) => {
        console.log('candidat: ', data);
        this.candidats = data;
      }
    )
  }

  sendData() {
    console.log(this.inscriptionForm.value);
    this.submitting = true;
    this._inscription.createInscription(this.inscriptionForm.value).subscribe(
      (data) => {
        console.log("Inscription reussie", data);
        setTimeout(
          () => {
            this.submitting = false
            window.location.reload()
          }, 1000
        )
      },
      (error) => {
        console.log("erreur durant l'inscription", error);
        setTimeout(
          () => {
            this.submitting = false
            window.location.reload()
          }, 1000
        )
      }
    )
  }
}
