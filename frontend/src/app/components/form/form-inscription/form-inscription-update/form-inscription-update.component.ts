import { Component } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { DynamicDialogRef } from 'primeng/dynamicdialog';
import { CandidatService } from 'src/app/services/candidat.service';
import { InscriptionService } from 'src/app/services/inscription.service';

@Component({
  selector: 'app-form-inscription-update',
  templateUrl: './form-inscription-update.component.html',
  styleUrls: ['./form-inscription-update.component.scss']
})
export class FormInscriptionUpdateComponent {
  inscription: any;
  codeCandidat!: number;
  montant!: number;
  candidat: any;
  submitting = false;

  ref: DynamicDialogRef | undefined;

  constructor(
    private formBuilder: FormBuilder,
    private _candidat: CandidatService,
    private _inscription: InscriptionService
  ) { }

  ngOnInit() {
    this.inscription = JSON.parse(localStorage.getItem('inscription') || '');
    console.log(this.inscription);

    this.codeCandidat = this.inscription.codeCandidat;
    this.montant = this.inscription.montant

    this._candidat.getCandidatById(this.inscription.codeEtudiant).subscribe(
      (data) => {
        console.log(data);
        this.candidat = data;
        this.codeCandidat = this.candidat.codeCandidat;
      }
    )
  }

  inscriptionForm = this.formBuilder.group({
    codeInscription: new FormControl(''),
    codeEtudiant: new FormControl('', [Validators.required]),
    montant: new FormControl('', [Validators.required])
  })

  sendData() {
    console.log(this.inscriptionForm.value);
    this.submitting = true
    this._inscription.updateInscription(this.inscriptionForm.value).subscribe(
      (data) => {
        console.log("Inscription reussie", data);
        setTimeout(
          () => {
            this.submitting = false
            window.location.reload()
          }, 1000
        )
      },
      (error) => {
        console.log("erreur durant l'inscription", error);
        setTimeout(
          () => {
            this.submitting = false
            window.location.reload()
          }, 1000
        )
      }
    )
  }
}
