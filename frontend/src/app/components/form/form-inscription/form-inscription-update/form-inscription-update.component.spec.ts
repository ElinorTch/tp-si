import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormInscriptionUpdateComponent } from './form-inscription-update.component';

describe('FormInscriptionUpdateComponent', () => {
  let component: FormInscriptionUpdateComponent;
  let fixture: ComponentFixture<FormInscriptionUpdateComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FormInscriptionUpdateComponent]
    });
    fixture = TestBed.createComponent(FormInscriptionUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
