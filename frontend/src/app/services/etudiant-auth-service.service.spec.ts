import { TestBed } from '@angular/core/testing';

import { EtudiantAuthServiceService } from './etudiant-auth-service.service';

describe('EtudiantAuthServiceService', () => {
  let service: EtudiantAuthServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EtudiantAuthServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
