import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environment/environment';

@Injectable({
  providedIn: 'root'
})
export class NoteService {

  constructor(
    private http: HttpClient
  ) { }

  createNote(codeMatiere: number, data: any) {
    return this.http.post(`${environment.URL_NOTE}/${codeMatiere}`, data);
  }

  updateNote(data: any) {
    return this.http.put(`${environment.URL_NOTE}`, data);
  }

  getAllNote() {
    return this.http.get(`${environment.URL_NOTE}`);
  }

  getAllMatiere() {
    return this.http.get(`${environment.URL_NOTE}/matiere`)
  }

  getNoteByEtudiant(codeEtudiant: number) {
    return this.http.get(`${environment.URL_NOTE}/${codeEtudiant}`)
  }
}
