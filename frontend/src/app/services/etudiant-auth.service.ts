import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environment/environment';

@Injectable({
  providedIn: 'root'
})
export class EtudiantAuthService {

  constructor(
    private http: HttpClient
  ) { }

  login(value: any) {
    return this.http.post(`${environment.URL_COMPOSITE}/login`, value)
  }
}
