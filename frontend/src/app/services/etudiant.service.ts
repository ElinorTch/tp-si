import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environment/environment';

@Injectable({
  providedIn: 'root'
})
export class EtudiantService {

  constructor(
    private http: HttpClient
  ) { }

  getAllEtudiant() {
    return this.http.get(`${environment.URL_COMPOSITE}/etudiant`);
  }

  createCandidat(value: any) {
    return this.http.post(`${environment.URL_ETUDIANT}/candidat`, value);
  }
}
