import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environment/environment';

@Injectable({
  providedIn: 'root'
})
export class InscriptionService {

  constructor(
    private http: HttpClient
  ) { }

  getAllInscription() {
    return this.http.get(`${environment.URL_INSCRIPTION}`);
  }

  createInscription(value: any) {
    return this.http.post(`${environment.URL_COMPOSITE}/inscription`, value);
  }

  updateInscription(value: any) {
    return this.http.put(`${environment.URL_INSCRIPTION}/updateInscription`, value);
  }

  getInscriptionByEtudiant(codeEtudiant: number) {
    return this.http.get(`${environment.URL_INSCRIPTION}/${codeEtudiant}`);

  }
}
