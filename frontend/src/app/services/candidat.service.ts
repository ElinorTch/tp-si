import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environment/environment';


@Injectable({
  providedIn: 'root'
})
export class CandidatService {

  constructor(
    private http: HttpClient
  ) { }

  getAllCandidat() {
    return this.http.get(`${environment.URL_ETUDIANT}/get`);
  }

  getCandidatById(id: number) {
    return this.http.get(`${environment.URL_ETUDIANT}/${id}`);
  }
  
  createCandidat(value: any) {
    return this.http.post(`${environment.URL_ETUDIANT}/create`, value);
  }

  updateCandidat(value: any) {
    return this.http.put(`${environment.URL_ETUDIANT}/update`, value);
  }
}
