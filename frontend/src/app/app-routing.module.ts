import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SidebarComponent } from './shared/components/sidebar/sidebar.component';
import { EtudiantsListComponent } from './components/listes/etudiants-list/etudiants-list.component';
import { CandidatListComponent } from './components/listes/candidat-list/candidat-list.component';
import { InscriptionListComponent } from './components/listes/inscription-list/inscription-list.component';
import { NoteListComponent } from './components/listes/note-list/note-list.component';
import { EtudiantAuthService } from './services/etudiant-auth.service';
import { EtudiantComponent } from './modules/authentication/etudiant/etudiant.component';
import { PersonnelComponent } from './modules/authentication/personnel/personnel.component';
import { AuthenticationComponent } from './modules/authentication/authentication.component';
import { ComponentsComponent } from './components/components.component';
import { AppComponent } from './app.component';

const routes: Routes = [
  {
    path: 'auth',
    component: AuthenticationComponent,
    loadChildren: ()=> import('./modules/authentication/authentication.module').then(m => m.AuthenticationModule)
  },
  {
    path: 'app',
    component: ComponentsComponent,
    loadChildren: ()=> import('./components/components.module').then(m => m.ComponentsModule)
  },
  {
    path: '',
    redirectTo: 'auth',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
