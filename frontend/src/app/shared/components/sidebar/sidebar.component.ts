import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {
  user: any;

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
      this.user = JSON.parse(localStorage.getItem("user") || '');
  }

  logout() {
    localStorage.clear()
    this.router.navigate(['/auth'])
  }
}
